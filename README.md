# FetchModule

A module for making HTTP requests based on fetch.

## Installation

```sh
npm install @isatol/fetchmodule
```

## Use

There two options to use the module, one of them are importing 3 functions independently, and the second way, is instantiate the class.

### The first method: importing the functions

```javascript
import { createInstance, useWithHeaders, request } from "@isatol/fetchmodule";
```

- With `createInstance()` you can pass as an argument a URI address that will be the basis for subsequent requests

```javascript
createInstance("https://reqres.in/api/");
```

- With `useWithHeaders()` you can pass as an argument the initial headers for all request

```javascript
let headers = new Headers();
headers.append("Content-Type", "application/json");
useWithHeaders(headers);
```

- With `request()` you can make HTTP request. If `createInstance()` is active and has an URI argument, just complete the rest of the URI what do you want to access.

```javascript
request("users", {
  method: "get",
});
request("https://reqres.in/api/users", {
  method: "get",
});
```

The second argument what receive `request()` is called `options`, which is a series of options to complete the request.

```javascript
request("users", {
  method: "" -> "get, post, put, delete",
  data: JSON.stringify({}) -> use it when the method is different from "get",
  headers: {} -> If `useWithHeaders()` is active, this part is autocomplete,
  params: {} -> Use it when the method is just "get",
  result: "" -> The value when te promise is resolved. "json, arrayBuffer, blob, text". Default is "json"
})
```

#### Example

```javascript
request("users", {
  method: "get",
  params: {
    page: 2,
  },
}).then((response) => {
  this.total = response.total;
});
```
